import { OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../../../services/data.service';

@Component({
  selector: 'app-delete-test-dialog',
  templateUrl: './delete-test-dialog.component.html',
  styleUrls: ['./delete-test-dialog.component.scss']
})
export class DeleteTestDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<DeleteTestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) { }

onNoClick(): void {
this.dialogRef.close();
}

confirmDelete(): void {
this.dataService.deleteIssue(this.data.id);
}

  ngOnInit() {
  }

}
