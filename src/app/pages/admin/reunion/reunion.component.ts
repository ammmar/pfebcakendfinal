import {Component, OnInit, ViewChild} from '@angular/core';
import { ScheduleComponent, EventSettingsModel,
  DayService, WeekService, WorkWeekService, MonthService, View, ActionEventArgs } from '@syncfusion/ej2-angular-schedule';

import {  ODataV4Adaptor, Query } from '@syncfusion/ej2-data';

import { ButtonComponent } from '@syncfusion/ej2-angular-buttons';
import {MatPaginator} from '@angular/material/paginator';
import { DataManager, WebApiAdaptor } from '@syncfusion/ej2-data';
import {HttpClient} from '@angular/common/http';
import {TestService} from '../../../services/test.service';
import {ReunionService} from '../../../services/reunion.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {AffecterTestComponent} from '../../../Modal/affecter-test/affecter-test.component';
import {AddReunionComponent} from '../../../Modal/add-reunion/add-reunion.component';
@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.scss']
})
export class ReunionComponent implements OnInit {




/*
  @ViewChild("scheduleObj", {static: true})
  public scheduleObj: ScheduleComponent;
  @ViewChild("addButton", {static: true})
  public addButton: ButtonComponent;
*/

  public selectedDate: Date = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDay());
  public scheduleViews: View =  'Month';
  public eventSettings: EventSettingsModel ;


  constructor(private httpClient: HttpClient,private reunionService:ReunionService,public dialog: MatDialog){
    this.reunionService.getAllReunion().toPromise().then(reponse=>{
     // console.log("reunion list =====>**"+JSON.stringify(reponse));
      let listReunion=JSON.parse(JSON.stringify(reponse));
      let ListReunionSettingData=[];
      for(let i=0;i<listReunion.length;i++){
       /* console.log("subject   "+listReunion[i].subject+"    startTime   year " +
          +listReunion[i].startTime.slice(0,10).slice(0,4)+"    start Time month "+
          listReunion[i].startTime.slice(0,10).slice(5,6));*/

        let tabreunion=listReunion[i].startTime.split("-");
        let tabreunionEnd=listReunion[i].endTime.split("-");

        /*
        console.log("subject   "+listReunion[i].subject+" start Time Year "+tabreunion[0]);
        console.log("subject   "+listReunion[i].subject+" start Time month "+tabreunion[1]);
        console.log("subject   "+listReunion[i].subject+" start Time day "+tabreunion[2].slice(0,2));*/

        console.log("subject   "+listReunion[i].subject+" start Time heure "+tabreunion[2].substr(3,2));
        console.log("subject   "+listReunion[i].subject+" start Time minute "+tabreunion[2].substr(6,2));

        console.log("subject   "+listReunion[i].subject+" start Time month "+tabreunion[1])

        /*
        console.log("subject   "+listReunion[i].subject+" end Time Year "+tabreunionEnd[0]);
      console.log("subject   "+listReunion[i].subject+" end Time month "+tabreunionEnd[1]);
        console.log("subject   "+listReunion[i].subject+" end Time day "+tabreunionEnd[2].slice(0,2));*/
        let m=tabreunion[2].slice(0,2);
        let m2=tabreunionEnd[2].slice(0,2);
        console.log("month start reunion "+m+"subject "+listReunion[i].subject);

        let reunion={
          Id: listReunion[i].id,
          Subject: listReunion[i].subject,
          StartTime:  new Date(tabreunion[0],tabreunion[1],m,tabreunion[2].substr(3,2),
            tabreunion[2].substr(6,2)),
          EndTime:  new Date(tabreunionEnd[0],tabreunionEnd[1],m2,tabreunion[2].slice(0,2),
            tabreunionEnd[2].substr(3,2), tabreunionEnd[2].substr(6,2)),
          IsAllDay: listReunion[i].isAllDay
        }
        ListReunionSettingData.push(reunion);
        tabreunion={};
        tabreunionEnd={};
        


      }

      this.eventSettings= {
        dataSource:ListReunionSettingData
      };
      console.log("data source setting"+JSON.stringify(this.eventSettings.dataSource));

    },error=>{
      console.log("reunion list error  =====>**"+JSON.stringify(error));

    })
  }

  addMettingDialog(){
    const dialogRef = this.dialog.open(AddReunionComponent, {
      width: '700px',
      height:'500px',


    });
  }

  ngOnInit() {
  }







  /*
= {
    dataSource: [
      {
        Id: 1,
        Subject: 'Explosion of Betelgeuse Star',
        StartTime: new Date(2020, 6, 15, 9),
        EndTime: new Date(2020, 6, 15, 11)
      }, {
        Id: 2,
        Subject: 'Thule Air Crash Report',
        StartTime: new Date(2020, 6, 12, 12),
        EndTime: new Date(2020, 6, 12, 14)
      }, {
        Id: 3,
        Subject: 'Blue Moon Eclipse',
        StartTime: new Date(2020, 6, 13, 9 ),
        EndTime: new Date(2020, 6, 13, 11)
      }, {
        Id: 4,
        Subject: 'Meteor Showers in 2018',
        StartTime: new Date(2020, 6, 14, 13),
        EndTime: new Date(2020, 6, 14, 14)
      }]
  }


  public selectedDate: Date = new Date(new Date().getFullYear(),new Date().getMonth()+1,new Date().getDay());
  public scheduleViews: View[] = ['Day', 'Week', 'WorkWeek', 'Month'];
  public eventSettings: EventSettingsModel = {
    dataSource: [{
      Id: 1,
      Subject: 'Testing',
      StartTime: new Date(2020, 6, 1, 9, 0),
      EndTime: new Date(2020, 6, 1, 12, 0),
      IsAllDay: false
      // year month date hours minutes
    }, {
      Id: 2,
      Subject: 'Vacation',
      StartTime: new Date(2020, 6, 3, 9, 0),
      EndTime: new Date(2020, 6, 3, 10, 0),
      IsAllDay: false
    }]
  }
  public onButtonClick(): void {
    let data: Object[] = [{
      Id: 3,
      Subject: 'Conference',
      StartTime: new Date(2020, 1, 12, 9, 0),
      EndTime: new Date(2020, 1, 12, 10, 0),
      IsAllDay: true
    }, {
      Id: 4,
      Subject: 'Meeting',
      StartTime: new Date(2020, 1, 15, 10, 0),
      EndTime: new Date(2020, 1, 15, 11, 30),
      IsAllDay: false
    }];
    this.scheduleObj.addEvent(data);
    this.addButton.element.setAttribute('disabled','true');
  }


  public onActionBegin(args: ActionEventArgs): void {
    const weekEnds: number[] = [0, 6];
    if(args.requestType == 'eventCreate' && weekEnds.indexOf((args.data[0].StartTime).getDay()) >= 0) {
      args.cancel = true;
    }
  }

  constructor(private httpClient: HttpClient,private reunionService:ReunionService){
    this.reunionService.getAllReunion().toPromise().then(reponse=>{
      console.log("reunion list =====>**"+JSON.stringify(reponse));
      let listReunion=JSON.parse(JSON.stringify(reponse));
      let ListReunionSettingData=[];
      for(let i=0;i<listReunion.length;i++){
        console.log("subject   "+listReunion[i].subject+"   startTime"+listReunion[i].startTime);
        let reunion={
          Id: listReunion[i].id,
          Subject: listReunion[i].subject,
          StartTime:  new Date(2020, 1, 15, 10, 0),
          EndTime: new Date(2020, 1, 15, 12, 0),
          IsAllDay: listReunion[i].isAllDay
        }
        ListReunionSettingData.push(reunion);


      }

      this.eventSettings= {
        dataSource:ListReunionSettingData
      };

    },error=>{
      console.log("reunion list error  =====>**"+JSON.stringify(error));

    })
  }
   */

}
