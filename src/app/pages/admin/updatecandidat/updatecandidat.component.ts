import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {CandidatService} from '../../../services/candidat.service';

export const formErrors: { [key: string]: string } = {
  required: 'champ obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Le mot de passe doit contenir au moins 8 caractères.\n.',
  minLengthPhone: 'Le numéro de téléphone doit contenir au moins 8 caractères.\n.',
  email:'format email est invalid \n',
  mismatch: 'Les mots de passe ne correspondent pas\n.',
  unique: 'Les mots de passe doivent contenir au moins 3 caractères uniques.\n.'
};
@Component({
  selector: 'app-updatecandidat',
  templateUrl: './updatecandidat.component.html',
  styleUrls: ['./updatecandidat.component.scss']
})
export class UpdatecandidatComponent implements OnInit {


  formErrors = formErrors;

  nom = new FormControl('', [Validators.required]);
  prenom = new FormControl('', [Validators.required]);
  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    niveau : new FormControl('', [Validators.required,Validators.min(0),
      Validators.max(2)]),
    tel : new FormControl('', [Validators.required,Validators.minLength(8),
      Validators.maxLength(8)]),
    experience : new FormControl('', [Validators.required,Validators.minLength(8),
      Validators.maxLength(8)])




  });

  ecole = new FormControl('', [Validators.required]);
  experience = new FormControl('', [Validators.required]);
  profil = new FormControl('', [Validators.required]);
  dateDebot = new FormControl('', [Validators.required]);



  updateCandidat:FormGroup;
  IsWait:any=false;
  redirectDelay: number = 0;
  idCandidat:any;
  candidatUpdated:any;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {rememberMe: true};
  today=new Date();
  email:any;
  showMessages: any = {};
  submitted: boolean = false;

  validation = {};
  constructor(private  candidatService:CandidatService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {

    this.idCandidat = this.activatedRoute.snapshot.params['id'];
    this.afficheMsq("id root "+this.idCandidat);
    if(this.idCandidat>0){
      this.candidatService.getCandidatById(this.idCandidat).toPromise().then(reponse=>{
        this.afficheMsq("candiat "+JSON.stringify(reponse));
        this.candidatUpdated=JSON.parse(JSON.stringify(reponse));
        this.afficheMsq("nom canidat"+this.candidatUpdated["nom"]);
      },error=>{
        this.afficheMsq("error===========> "+JSON.stringify(error));

      })
    }
  }

  ngOnInit() {



    this.updateCandidat=this.fb.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      email:['',Validators.email],
      ecole:['',Validators.required],
      tel:['',Validators.required],
      experience:['',Validators.required],
      niveau:['',Validators.required],
      profil:['',Validators.required],
      dateDebot:['',Validators.required]
    });
  }
  OnReset(){
    this.updateCandidat.reset();
  }

  onSubmit(){
    if(this.updateCandidat.valid)
    {
      this.IsWait=true;

      this.updateCandidat["idCandidat"]=this.idCandidat;
      this.afficheMsq("id"+ this.updateCandidat["idCandidat"])
      this.afficheMsq("Valid form "+this.updateCandidat.value["nom"]);
     let  date:Date =this.updateCandidat.value["dateDebot"];
      this.updateCandidat.value["pasword"]="";
      this.updateCandidat.value["month"]=this.getCurrentMonth(date.getMonth()+1);
     this.afficheMsq("updated current date is "+date.getMonth()+1);

      this.candidatService.creerModifierCandidat(this.updateCandidat.value,this.idCandidat).toPromise().then(response=>{
        this.afficheMsq("add candiat "+JSON.stringify(response));

        Swal.fire({
          title: 'update candidat success  ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(['/candidat/']);
      },error=>{
        Swal.fire({
          title: 'update candidat failure ',
          text: 'probleme d ajout candidat',
          icon: 'warning',
        });
        console.log("addTest:Erreur is =========>***"+JSON.stringify(error));
      });
    }
  }
  NavToList(){
    this.router.navigate(['/candidat/']);

  }

  afficheMsq(msg:any)
  {
    console.log("update-candiat      "+msg);
  }


  getCurrentMonth(month:number):string
  {
    switch (month) {
      case 1:
        return "Janvier"
        console.log("It is a janvier.");
        break;
      case 2:
        return "Fevrier";
        console.log("It is a Fevrier.");
        break;
      case 3:
        return "Mars";
        console.log("It is a Tuesday.");
        break;
      case 4:
        return "Avril";
        console.log("It is a Wednesday.");
        break;
      case 5:
        return "Mai";
        console.log("It is a Mai.");
        break;
      case 6:
        return "Juin";

        console.log("It is a Juin.");
        break;
      case 7:
        return "Juillet";
        console.log("It is a Saturday.");
        break;
      case 8:
        return "aout";
        console.log("It is a Saturday.");
        break;
      case 9:
        return "septembre";
        console.log("It is a Saturday.");
        break;
      case 10:
        return "octobre";
        console.log("It is a Saturday.");
        break;
      case 11:
        return "novembre";
        console.log("It is a Saturday.");
        break;
      case 12:
        return "décembre";
        console.log("It is a Saturday.");
        break;
      default:
        console.log("No such day exists!");
        break;
    }
  }

}
