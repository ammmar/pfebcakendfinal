import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListquestionSelectComponent } from './listquestion-select.component';

describe('ListquestionSelectComponent', () => {
  let component: ListquestionSelectComponent;
  let fixture: ComponentFixture<ListquestionSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListquestionSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListquestionSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
