import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import { TestService } from 'app/services/test.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {QuestionChoixComponent } from 'app/Modal/question-choix/question-choix.component';
import Swal from 'sweetalert2';
import {QuestionService} from '../../../services/question.service';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'app-listquestion-select',
  templateUrl: './listquestion-select.component.html',
  styleUrls: ['./listquestion-select.component.scss']
})
export class ListquestionSelectComponent implements OnInit {

  // myControls = new FormControl();
  IsWait:boolean=false;
  filteredOptions: Observable<string[]>
  idTest:any;
  listQuestion:any[]=[];
  listQuestionSelected:[]=[];

  myControl = new FormControl();
  //test:FormGroup;
  panelOpenState: boolean=false;
  options:[] = [];
  name: string;
  color: string;
  public searchText : string;
  Ischecked:boolean;
  nameTest:any;

 public  seletdQuestion:any[]=[];

  master_checked: boolean = false;
  master_indeterminate: boolean = false;



  constructor(private httpClient: HttpClient,private testService:TestService
              ,private router:Router,private activatedRoute: ActivatedRoute,public dialog: MatDialog,private questionService:QuestionService) {
    this.idTest = this.activatedRoute.snapshot.params['id'];
    console.log("id routed  " + this.idTest);
    this.nameTest=localStorage.getItem("nameTest");
    this.questionService.getAllQuestionUnique().toPromise().then(response => {
      console.log("qestionQuestionresponse is " +JSON.stringify( response));
      this.listQuestion = (JSON.parse(JSON.stringify(response)));
      for (let i = 0; i < this.listQuestion.length; i++) {
        let q = this.listQuestion[i];
        console.log("id question" + q["idQuestion"] + "desgn" + q["desgnQuestion"]);
      }
    });
  }

    ngOnInit() {
  }



  NaToUpdateQuestion(idQuestion:any){
    this.afficheMsg("id_question"+idQuestion);
    localStorage.setItem("idTestQuestion",this.idTest);
    this.router.navigate(['/update-question',idQuestion])
  }

  deleteDialog(idQuestion:any){


    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'voulez vous supprimer cette question ?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer ',
      cancelButtonText: 'No, cancel   !',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.questionService.DeleteQuestion(idQuestion).toPromise().then(reponse=>{
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'votre question est supprimé',
            'success'
          )
        },error=>{
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'votre question est supprime',
            'success'
          )
        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'votre question est en cours :)',
          'error'
        )
      }
    })







  }




  master_change() {
    for (let i=0;i<this.listQuestion.length;i++) {
      let value=this.listQuestion[i];
      value.checked = this.master_checked;

    }
  }

  affecterQuestionTest(){

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'voulez vous affecter ces  question ?',
      text: "vous devez affecter ces question aux test "+this.nameTest,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Vous etes sure !  ',
      cancelButtonText: 'Non, Annuler   !',
      reverseButtons: true

    }).then((result) => {

      if (result.value) {
        this.IsWait=true;
        for (let i = 0; i < this.listQuestion.length; i++) {

          let value:any = this.listQuestion[i];
          if(value!==null){
            if (value.checked) {
              value.idQuestion = 0;
              let choixs = value.choixs;
              for (let j = 0; j < choixs.length; j++) {
                choixs[j].idChoix = 0;
                choixs[j].reponse = 0;
              }
              value.choixs = choixs;
              this.seletdQuestion.push(value);
              this.afficheMsg("question" + i + "==>" + JSON.stringify(value));
            }
          }

        }
        this.questionService.affecterQuestionToTest(this.seletdQuestion,this.idTest).toPromise().then(reponse=>{
          swalWithBootstrapButtons.fire(
            'Affecter!',
            'affectation question avec Réussite ',
            'success'
          )
          this.IsWait=false;
        },error=>{
          swalWithBootstrapButtons.fire(
            'Probleme !',
            'probleme d affectation',
            'warning'
          )
        });
          this.IsWait=false;


      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Opération annuler :)',
          'error'
        )
      }
    })




  }


  list_change() {
    let checked_count = 0;
    //Get total checked items
    for (let i = 0; i < this.listQuestion.length; i++) {

      let value = this.listQuestion[i];
     // this.afficheMsg(" question "+i+"==>"+JSON.stringify(value)+"\n");

      if (value.checked)
        checked_count++;
    }

    if (checked_count > 0 && checked_count < this.listQuestion.length) {
      // If some checkboxes are checked but not all; then set Indeterminate state of the master to true.
      this.master_indeterminate = true;
    } else if (checked_count == this.listQuestion.length) {
      //If checked count is equal to total items; then check the master checkbox and also set Indeterminate state to false.
      this.master_indeterminate = false;
      this.master_checked = true;
    } else {
      //If none of the checkboxes in the list is checked then uncheck master also set Indeterminate to false.
      this.master_indeterminate = false;
      this.master_checked = false;
    }


  }
    afficheMsg(msg:any){
    console.log("*****GestionQuestionComponents******"+msg);
  }


}
