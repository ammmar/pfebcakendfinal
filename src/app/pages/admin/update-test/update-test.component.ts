

import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';
import Swal from "sweetalert2";

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup,Validators} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";

import {MatSelectChange} from '@angular/material';

//import{ShowcaseDialogComponent} from "../modal-overlays/dialog/showcase-dialog/showcase-dialog.component"

import { TestService } from 'app/services/test.service';
import { JsonPipe } from '@angular/common';
export const formErrors: { [key: string]: string } = {
  required: 'cette champs est obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Password must contain at least 8 characters.',
  minLengthPhone: 'Phone Number must contain at least 8 characters.',

  mismatch: 'Passwords don\'t match.',
  unique: 'Passwords must contain at least 3 unique characters.'
};
@Component({
  selector: 'app-update-test',
  templateUrl: './update-test.component.html',
  styleUrls: ['./update-test.component.scss']
})
export class UpdateTestComponent implements OnInit {
  formErrors = formErrors;

  idTest:any;
  testUpdated:any;
  modifierTest:FormGroup;
  listType: Array<string> = ["Technique","Logique"];
  public default:string="Technique";
  theme = new FormControl('', [Validators.required]);
  description = new FormControl('', [Validators.required]);
  dateDebut = new FormControl('', [Validators.required]);
  dateFin = new FormControl('', [Validators.required]);
  type= new FormControl('', [Validators.required]);

  constructor(private activatedRoute:ActivatedRoute, private fb:FormBuilder,private router:Router,private testService:TestService) {
    this.idTest=this.activatedRoute.snapshot.params['id'];
    console.log("id routed  "+this.idTest);

    this.testService.getTestById(this.idTest).toPromise().then(response=>{
      // this.ListTest=res.json();
      //const usersJson: any[] = Array.of(res.json());
      console.log("response is "+response)
      this.testUpdated=(JSON.parse(JSON.stringify(response)));
      this.default=this.testUpdated["typeTest"];
      console.log("test updatetd theùme is "+this.testUpdated["theme"])

   });
  }
  ngOnInit() {
    this.modifierTest=this.fb.group({
      theme:['',Validators.required],
      description:['',Validators.required],
      dateDebut:['',Validators.required],
      dateFin:['',Validators.required],
      type:['',Validators.required],
      typeTest:['',Validators.required]
    });
  }


  typeSelected($event: MatSelectChange) {
    this.modifierTest.patchValue({
      typeTest:$event.value
    })
  }
  onSubmit(){

    if(this.modifierTest.valid){
      this.modifierTest.value["idTest"]=this.idTest;
        this.testService.modifierTest(this.modifierTest.value,2).toPromise().then(response=>{
          let test=JSON.parse(JSON.stringify(response));
            if(test.idTest==this.idTest){
              Swal.fire({
                title: 'modifier test success  ',
                text: 'success',
                icon: 'success',
            });
              this.router.navigate([("/test")])
            }
            else{
              Swal.fire({
                title: 'modifier test echec  ',
                text: 'failure',
                icon: 'success',
            });
              this.router.navigate([("/test")])

            }

        },error=>{
            console.log("error de modification de test "+JSON.stringify(error));
        });

    }



    /*
      this.testService.ajouterTest(this.addtest.value,2).toPromise().then(response=>{

    */
  }

}
