import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReunionService {

  private baseUrl = 'http://localhost:9093/auth/reunion';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }


  getAllReunion():Observable<Object>{
    return this.httpClient.get(`${this.baseUrl}/getAllReunion`);
  }

  saveReunion(reunion:any,id:any):Observable<Object>{
    return this.httpClient.put(`${this.baseUrl}/addreunion/${id}`,reunion);
  }

}
