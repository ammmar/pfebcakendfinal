import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Test} from '../models/test';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  private baseUrl = 'http://localhost:9093/auth/test';
  private readonly API_URL_Etat = 'http://localhost:9093/auth/test';
  dataChange: BehaviorSubject<Test[]> = new BehaviorSubject<Test[]>([]);


  constructor(private httpClient:HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });


   }
  get data(): Test[] {
    return this.dataChange.value;
  }
gettetsme():Observable<Object>{
  return this.httpClient.get<Test[]>(`${this.baseUrl}/getAllTest/etatTest`)
}

    getAllTestEtat():void{
      this.httpClient.get<Test[]>(this.API_URL_Etat+'/getAllTest/etatTest').subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
    }

   getallTest():Observable<Object>{
     return this.httpClient.get(`${this.baseUrl}/getAllTest`);
   }
   getTestById(id:any):Observable<Object>{
    return  this.httpClient.get(`${this.baseUrl}/getTestById/${id}`);
   }
   ajouterTest(test:any,id:any):Observable<Object>{
     //console.log("id"+id);
    // console.log("test"+test)
    //{params:new HttpParams().set('id',id)}
    return this.httpClient.put(`${this.baseUrl}/creerTest/${id}`,test);
  }

  modifierTest(test:any,id:any):Observable<Object>{
   return this.httpClient.put(`${this.baseUrl}/modifierTest/${id}`,test);
 }

 getQuestionByTestId(id:any):Observable<Object>{
  // return this.httpClient.put(`${this.baseUrl}/creerTest/${id}`,test);

  return this.httpClient.get(`${this.baseUrl}/getQuestionByIdTest/${id}`);
  //modifierTest
}

affecterTestCandidat(id_test:any,id_candidat:any,test:any):Observable<Object>{
  return this.httpClient.put(`${this.baseUrl}/affecterTeste/${id_test}/${id_candidat}`,test);

}
deleteTest(id:any):Observable<Object>{
  return this.httpClient.delete(`${this.baseUrl}/deleteTest/${id}`);

}



}
