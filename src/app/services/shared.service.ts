import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  messageSource: BehaviorSubject<string>;
  currentMessage = "";

  constructor() {
    this.messageSource=new BehaviorSubject(this.currentMessage)
  }

  changeMessage(message: string) {
     this.messageSource.next(message);
  }

}
