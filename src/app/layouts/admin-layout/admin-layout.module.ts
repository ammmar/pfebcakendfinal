import {Injectable, NgModule} from '@angular/core';
import {RouterModule, UrlSerializer} from '@angular/router';
import {CommonModule, LowerCasePipe} from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";

import {ToasterService} from "angular2-toaster";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';


import { DashboardComponent }       from '../../pages/admin//dashboard/dashboard.component';
import { UserComponent }            from '../../pages/admin/user/user.component';
import { TableComponent }           from '../../pages/admin/table/table.component';
import { TypographyComponent }      from '../../pages/admin/typography/typography.component';
import { IconsComponent }           from '../../pages/admin/icons/icons.component';
import { MapsComponent }            from '../../pages/admin/maps/maps.component';
import { NotificationsComponent }   from '../../pages/admin/notifications/notifications.component';
import { UpgradeComponent }         from '../../pages/admin/upgrade/upgrade.component';
import { AddquestionComponent } from '../../pages/admin/addquestion/addquestion.component';


import { ListcandidatComponent }         from '../../pages/admin/listcandidat/listcandidat.component';

import { ListTestComponent}         from '../../pages/admin/list-test/list-test.component';
import { AddTestComponent } from '../../pages/admin/add-test/add-test.component';

import {  UpdateTestComponent} from '../../pages/admin/update-test/update-test.component';
import {  GestionquestionComponent} from '../../pages/admin/gestionquestion/gestionquestion.component';



import { DayService, WeekService, DragAndDropService,WorkWeekService, MonthService, AgendaService,MonthAgendaService } from '@syncfusion/ej2-angular-schedule';

import {NbCardModule, NbToastrService} from '@nebular/theme';

import { NbThemeModule} from '@nebular/theme';
import {
 NbDialogModule,
  NbDialogService,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule
} from '@nebular/theme';


import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,

  MatDatepickerModule,
  MatDividerModule, MatExpansionModule,
  MatFormFieldModule, MatIconModule,
  MatSelectModule,
  MatInputModule, MatNativeDateModule, MatSlideToggleModule, MatTooltipModule
} from "@angular/material";
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatRadioModule} from '@angular/material/radio';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule} from '@angular/forms';
import {QuestionService} from '../../services/question.service';
import {TestService} from '../../services/test.service';
import {UpdatequestionComponent} from '../../pages/admin/updatequestion/updatequestion.component';
import {CandidatService} from '../../services/candidat.service';
import {AddCandidatComponent} from '../../pages/admin/add-candidat/add-candidat.component';
import {UpdatecandidatComponent} from '../../pages/admin/updatecandidat/updatecandidat.component';
import {QuestionChoixComponent} from '../../Modal/question-choix/question-choix.component';
import {ChoixService} from '../../services/choix.service';
import {SimulerTestComponent} from '../../pages/admin/simuler-test/simuler-test.component';
import {SimulerTestService} from '../../services/simuler-test.service';
import {GrdFilterPipe} from '../../Utility/GrdFilterPipe';
import {AffecterTestComponent} from '../../Modal/affecter-test/affecter-test.component';
import {ChartsModule} from 'ng2-charts';
import {StatService} from '../../services/stat.service';
import {EtatTestComponent} from '../../pages/admin/etat-test/etat-test.component';
import {MailComposeDialogComponent} from '../../Modal/mail-compose-dialog/mail-compose-dialog.component';
import {ContactService} from '../../services/contact.service';
import {VoirResultatComponent} from '../../pages/admin/voir-resultat/voir-resultat.component';
import {SharedService} from '../../services/shared.service';
import {NgxPrintModule} from 'ngx-print';
import {ListUserComponent} from '../../pages/admin/list-user/list-user.component';
import {UserService} from '../../services/user.service';
import {AddUserComponent} from '../../pages/admin/add-user/add-user.component';
import {UpdateUserComponent} from '../../pages/admin/update-user/update-user.component';
import {ReunionComponent} from '../../pages/admin/reunion/reunion.component';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import {ReunionService} from '../../services/reunion.service';
import {AddReunionComponent} from '../../Modal/add-reunion/add-reunion.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { MatSliderModule } from '@angular/material/slider';

import {
  NGX_MAT_DATE_FORMATS, NgxMatDateAdapter,
  NgxMatDateFormats,
  NgxMatDatetimePickerModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {NgxMaterialTimepickerContainerComponent} from 'ngx-material-timepicker/src/app/material-timepicker/components/ngx-material-timepicker-container/ngx-material-timepicker-container.component';
import {MatProgressBar, MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {AffecterCandidatTestComponent} from '../../Modal/affecter-candidat-test/affecter-candidat-test.component';
import { ListquestionSelectComponent } from 'app/pages/admin/listquestion-select/listquestion-select.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatListModule} from '@angular/material/list';

// If using Moment
const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: "l, LTS"
  },
  display: {
    dateInput: "l, LTS",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NbThemeModule,
    ButtonModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatRadioModule,
    NgxPrintModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    HttpClientModule,
    NbThemeModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    NgbModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTooltipModule,
    NbCardModule,
    MatSelectModule,
    ChartsModule,
    ScheduleModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMaterialTimepickerModule,
    NgbModalModule,
    MatProgressBarModule,
    MatSliderModule
    , MatProgressSpinnerModule,
    MatCheckboxModule,
    MatListModule

  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    TableComponent,
    UpdatequestionComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    AddTestComponent,
    UpdatecandidatComponent,
    ListcandidatComponent,
    NotificationsComponent,
    UpdateTestComponent,
    ListTestComponent,
    AddquestionComponent,
    GestionquestionComponent,
    AddCandidatComponent,
    QuestionChoixComponent,
    SimulerTestComponent,
    GrdFilterPipe,
    AffecterTestComponent,
    EtatTestComponent,
    MailComposeDialogComponent,
    VoirResultatComponent,
    ListUserComponent,
    AddUserComponent,
    UpdateUserComponent,
    ReunionComponent,
    AddReunionComponent,
    AffecterCandidatTestComponent,
    ListquestionSelectComponent
  ],

  providers:[QuestionService,TestService,StatService,NbToastrService,ToasterService,
    NbDialogService,CandidatService,ChoixService,SimulerTestService
  ,ContactService,SharedService,UserService,DayService,
    WeekService,
    WorkWeekService,
    MonthService,
    AgendaService,
    MonthAgendaService,
    , DragAndDropService,
    ReunionService,{
    provide:UrlSerializer,
      useClass:LowerCasePipe
    }
  ],
  exports: [AddReunionComponent ]
,
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [QuestionChoixComponent,AffecterTestComponent,MailComposeDialogComponent,
    AddReunionComponent, AffecterCandidatTestComponent
   ],
})


export class AdminLayoutModule {}
