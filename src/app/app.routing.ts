import {ExtraOptions, RouterModule, Routes} from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {LoginPageComponent} from './auth/login-page/login-page.component';
import {LoginActivate} from './auth/loginActivate';
import {AuthGuardService} from './auth/auth-guard.service';
import {NgModule} from '@angular/core';



export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  }]},
  {
    path: '**',
    redirectTo: 'dashboard'
  }
]




  /*

    { path: 'dashboard',canActivate: [AuthGuardService], loadChildren: () => import('./layouts/admin-layout/admin-layout.module')
      .then(m => m.AdminLayoutModule),
  },
  { path: 'auth', loadChildren: () => import('./auth/login-page/login-page.module')
      .then(m => m.LoginPageModule),
  },

  { path: '**', redirectTo: 'dashboard'  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'  },



*/


///auth/login-page/login-page.module
/*
const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
*/
