import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from "sweetalert2";
import {ContactService} from '../../services/contact.service';
import {Router} from '@angular/router';

import { MatProgressBarModule } from '@angular/material/progress-bar';

export const formErrors: { [key: string]: string } = {
  required: 'champ obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Le mot de passe doit contenir au moins 8 caractères.\n.',
  minLengthPhone: 'Le numéro de téléphone doit contenir au moins 8 caractères.\n.',
  email:'format email est invalid \n',
  mismatch: 'Les mots de passe ne correspondent pas\n.',
  unique: 'Les mots de passe doivent contenir au moins 3 caractères uniques.\n.'
};

@Component({
  selector: 'app-mail-compose-dialog',
  templateUrl: './mail-compose-dialog.component.html',
  styleUrls: ['./mail-compose-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class MailComposeDialogComponent implements OnInit {
  namec: any;
  idTest:any;
  IsWait:any=false;
  formErrors= formErrors;

  emailc:any;

    email= new FormControl('', [Validators.required, Validators.email]);

  subject = new FormControl('', [Validators.required]);
  message = new FormControl('', [Validators.required]);
  name = new FormControl('', [Validators.required]);

  composeForm:FormGroup;

  /**
   * Constructor
   *
   * @param {MatDialogRef<MailNgrxComposeDialogComponent>} matDialogRef
   * @param _data
   * @param {FormBuilder} _formBuilder
   */
  constructor(public  dialogRef: MatDialogRef<MailComposeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any, private _formBuilder: FormBuilder
    , private contactService: ContactService,private router:Router) {
    this.namec = this._data.name;
    this.idTest=_data.idTest;
    this.emailc=this._data.email;
    this.afficheMessage("name " + this._data.name);
    // Set the defaults

    this.composeForm = this.createComposeForm();
  }

  ngOnInit() {
  }

  createComposeForm(): FormGroup {
    return this._formBuilder.group({
      from: {
        value: ['ammar.hamza1995@gmail.com'],
        disabled: [true]
      },
      email: ['',Validators.required],
      subject: ['',Validators.required],
      name: ['',Validators.required],
      message: ['']
    });

  }

  afficheMessage(msg: any) {
    console.log("********** mail box dilaog****" + msg);
  }
  OnReset(){
   // this.composeForm.reset();
    this.composeForm.reset();
    Swal.close();
    this.dialogRef.close();

  }
  sendMail() {
    if (this.composeForm.valid) {
      this.IsWait=true;
      //let message= this.composeForm.value["message"]+"   http://localhost:4300/passerTest/"+this.idTest;
      //this.afficheMessage("form value"+this.composeForm.value);
     // this.composeForm.value["message"]= "";
      if(this.idTest>0){
      this.composeForm.value["message"]= " vous etes invité de passer le test suivante " +
        "   http://localhost:4300/passerTest/"+this.idTest;
      }

      this.contactService.SendMail(this.composeForm.value).toPromise().then(reponse => {

        this.IsWait=false;

        Swal.fire({
          title: 'success email  sent ',
          text: 'success',
          icon: 'success',
        });
        //this.dialogRef.close();
        this.composeForm.reset()
        this.dialogRef.close();

      },error=>{
       // this.router.navigate(["/etatTest"]);
        Swal.fire({
          title: 'mail non evoyeé  ',
          text: 'problem ',
          icon: 'warning',
        })
       // this.dialogRef.close();
        this.composeForm.reset();
        this.dialogRef.close();
      })

    }else if(this.composeForm.invalid){
      this.dialogRef.close();
      this.router.navigate(["etatTest"]);
    }


  }
}
