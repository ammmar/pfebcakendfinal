import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffecterCandidatTestComponent } from './affecter-candidat-test.component';

describe('AffecterCandidatTestComponent', () => {
  let component: AffecterCandidatTestComponent;
  let fixture: ComponentFixture<AffecterCandidatTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffecterCandidatTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffecterCandidatTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
