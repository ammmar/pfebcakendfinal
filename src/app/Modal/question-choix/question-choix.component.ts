import {Component, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ModalDataChoix } from 'app/Modal/data/model-data-choix';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import 'style-loader!angular2-toaster/toaster.css';
import {NbDialogService} from "@nebular/theme";
import {MatSelectChange} from '@angular/material';
import {QuestionService} from '../../services/question.service';
import Swal from "sweetalert2";
import {ChoixService} from '../../services/choix.service';
import {MatRadioButton, MatRadioChange} from '@angular/material/radio';

@Component({
  selector: 'app-question-choix',
  templateUrl: './question-choix.component.html',
  styleUrls: ['./question-choix.component.scss']
})
export class QuestionChoixComponent implements OnInit {
choixForm:FormGroup;
addchoixForm:FormGroup;
  nameControl = new FormControl('');
buttsonState=false;
  myModel="";

currentchoix:any;
listchoix:{ idChoix: number, choix: string,reponse:number }[]=[{"idChoix":1,"choix":"emptychoix","reponse":0}] ;
  choixfinal:{ idChoix: number, choix: string,reponse:number };
  @Output()
  change: EventEmitter<MatRadioChange>
//=[{"idChoix":1,"choix":"not found","reponse":0}];
choixc:any;
id_question:any;
name:any;
messageInfo:any;
  filter: any;
  choix= new FormControl('', [Validators.required]);

  //
selectedchoix :{ idChoix: number, choix: string,reponse:number };

 // selectedchoix :any;
arrychoix:[]=[];
  constructor(public dialogRef: MatDialogRef<QuestionChoixComponent>, @Inject(MAT_DIALOG_DATA) public data: ModalDataChoix,private  fb: FormBuilder, private questionService: QuestionService,private choixService:ChoixService)
  {
    this.id_question=this.data.id_question;
    this.name=this.data.name;
    this.affichemsg("origin dilaog choix  id question  "+this.id_question);
    this.affichemsg("origin dilaog choix  name  "+this.name);
    this.questionService.getChoixByQuestionId(this.id_question).toPromise().then(response=>{
      this.affichemsg("choix selected"+JSON.stringify(response));
      this.listchoix=JSON.parse(JSON.stringify(response));
      let choix=this.listchoix[0];
      this.affichemsg("choix try "+choix["choix"]+"rep ===>"+choix["reponse"]);
      for(let i=0;i<this.listchoix.length;i++) {
        let ch=this.listchoix[i];
         if(ch["reponse"]===1){
          this.selectedchoix={"idChoix":ch["idChoix"],"choix":ch["choix"],"reponse":1};
        }

      }


    },error=>{
      this.affichemsg("origin dilaog choix   error"+JSON.stringify(error));
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
this.addchoixForm=this.fb.group({
  choix:['',Validators.required]
});
  }



//add new choix
  addNewChoix()
  {

    if(this.addchoixForm.valid) {
      //this.choixc["idChoix"]=0;

      let c=this.addchoixForm.value["choix"];
      this.affichemsg("current choix"+c);
      this.choixc={"idChoix":0,"choix":c,"reponse":0};
      this.listchoix.push(this.choixc);
      this.affichemsg(" new element added to array "+this.listchoix.length);

      this.choixService.creerChoixByIdQuestion(this.choixc,this.id_question).toPromise().then(response=>{
        this.myModel="";
        this.addchoixForm.value["choix"]="";
        this.affichemsg(" choix added  successfluy"+JSON.stringify(response));
        if(this.listchoix[0].choix==="emptychoix")
        {
          this.listchoix.splice(0,1);
        }
        this.messageInfo="choix added  successfluy";
      },error=>{
        this.affichemsg("add choix error"+JSON.stringify(error));
        this.messageInfo="error adding choix";
      });
      /*
       Swal.fire({
          title: 'add test success  ',
          text: 'success',
          icon: 'success',
        });
       */


    }
    else{
      this.affichemsg(" empty element  ");
    }
  }
  noclick(choix:any){
    this.affichemsg("no click event ");
    if(choix["reponse"]===1){
      this.affichemsg("true state");
      for(let i=0;i<this.listchoix.length;i++)
      {
        let c =this.listchoix[i];
        c["reponse"]=0;
        this.listchoix[i]=c;
      }
    }
    else{
      this.affichemsg("false state");
    }

    this.afficheArrayChoix("onNoClick");

  }



//detreminer selected choice
  displayChoice(choix:any,rep:any){
   // this.selectedchoix["reponse"] = 1;
   //this.selectedchoix=  {"idChoix":1,"choix":choix.choix,"reponse":1}
    this.selectedchoix={"idChoix":choix["idChoix"],"choix":choix["choix"],"reponse":rep};
    if(rep===1){
      this.selectedchoix["reponse"]=0;
    }
    else {
      this.selectedchoix["reponse"]=1;

    }
    this.affichemsg("*************choix selectionee " + "choix " + this.selectedchoix["choix"] + "reponse" + this.selectedchoix["reponse"]);

    for (let i = 0; i < this.listchoix.length; i++) {
      let c = this.listchoix[i];
      if (c["choix"] === this.selectedchoix["choix"]) {
        this.selectedchoix["reponse"]=1;
        c["reponse"]=1;
        this.listchoix[i] = c;
      }
      else if(c["choix"]!==this.selectedchoix["choix"] && c["reponse"]===1){
        this.listchoix[i].reponse=0;
      }

    }
    this.afficheArrayChoix("Onclick");
  }



  afficheArrayChoix(msg:any){

    for(let i=0;i<this.listchoix.length;i++) {
      let c=this.listchoix[i];
      this.affichemsg(msg+"id choix  "+c["idChoix"]+"  choix "+c["choix"]+"   reponse "+c["reponse"]+"  \n " )
    }


  }

  statButton(){
    if(this.myModel.length===0){
      return true;
    }else{
      return false;
    }
  }

  onBlurMethod(){
    alert(this.myModel)

    console.log("selected model "+this.myModel);
  }

  onFoucsMethod(){

  }

//enregister le noubeau etat des choix dans la base de donnees
  onSubmit(){


    this.choixService.EnregistrerStateQuestionChoix(this.selectedchoix,this.id_question).toPromise().then(reponse=>{
      this.affichemsg("reponse"+JSON.stringify(reponse));

      Swal.fire({

        icon: 'success',
        title: 'Succes Mise jour ',
        showConfirmButton: false,
        timer: 1500
      })

    },error=>{
      this.affichemsg("error "+JSON.stringify(error));
      Swal.fire({

        icon: 'warning',
        title: 'probleme de mise a jour ',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }


  affichemsg(msg:any)
  {
    console.log("questionChoixModal     "+msg);
  }
}
